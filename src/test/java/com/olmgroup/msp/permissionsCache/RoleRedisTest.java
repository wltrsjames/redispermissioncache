package com.olmgroup.msp.permissionsCache;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.olmgroup.msp.permissionsCache.model.Right;
import com.olmgroup.msp.permissionsCache.model.Role;
import com.olmgroup.msp.permissionsCache.services.PermissionRepository;
import com.olmgroup.msp.permissionsCache.services.PermissionRepositoryImpl;
import com.olmgroup.msp.permissionsCache.services.RoleService;
import com.olmgroup.msp.permissionsCache.services.RoleServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {RightsCacheApplication.class, RoleServiceImpl.class, PermissionRepositoryImpl.class}, loader=AnnotationConfigContextLoader.class)
@ActiveProfiles(profiles = "TEST")
public class RoleRedisTest {
	
	@Inject
	RoleService roleService;
	
	@Inject
	PermissionRepository permissionRepository;
	
  @Before 
  public void initialize() {
    permissionRepository.delete("ROLE", "Admin");
    permissionRepository.delete("ROLE", "Manager");
    
    Right right = new Right("Billing_Add", "BILLING.ADD");
    HashMap<String, Right> rightmap = new HashMap<String, Right>();
    rightmap.put(right.getidName(), right);
    
    Role role = new Role("Admin",rightmap);
    permissionRepository.save("ROLE", role);
  }
	
  @Test
  public void getData() throws Exception {
    Role role = roleService.getRoleByIdName("Admin");
    
    assertThat("Role should have a name", role.getidName(), equalTo("Admin"));
    assertThat("Role should have a Rights", role.getRights().size(), equalTo(1));
  }
  
  @Test
  public void saveData() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    HashMap<String, Right> rightmap = new HashMap<String, Right>();
    rightmap.put(right.getidName(), right);
    Role role = new Role("Manager", rightmap);
    
    roleService.save(role);
    Role returnedRole = permissionRepository.findByIdName("ROLE", "Manager");
    
    assertThat("Role should have a name", returnedRole.getidName(), equalTo("Manager"));
    permissionRepository.delete("ROLE", "Manager");
  }
  
  @Test
  public void updateData() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    HashMap<String, Right> rightmap = new HashMap<String, Right>();
    rightmap.put(right.getidName(), right);
    
    Role role = new Role("Manager", rightmap);
    permissionRepository.save("ROLE", role);
    
    Right rightUpdate = new Right("Billing_Add", "BILLING.ADD"); 
    HashMap<String, Right> rightUpdateMap = new HashMap<String, Right>();
    rightUpdateMap.put(right.getidName(), rightUpdate);
    
    Role roleUpdate = new Role("Manager", rightUpdateMap);
    roleService.update(roleUpdate);
    Role returnedRole = permissionRepository.findByIdName("ROLE", "Manager");
    
    assertThat("Right should have a name", returnedRole.getRights().values().iterator().next().getidName(), equalTo("Billing_Add"));
    permissionRepository.delete("ROLE", "Manager");
  }
  
  @Test
  public void deleteData() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    HashMap<String, Right> rightMap = new HashMap<String, Right>();
    rightMap.put(right.getidName(), right);
    
    Role role = new Role("Manager", rightMap);
    permissionRepository.save("ROLE", role);
    
    roleService.delete("Manager");
    Role returnedRole = permissionRepository.findByIdName("ROLE", "Manager");
    
    assertThat("Role should not exist", returnedRole, equalTo(null));
    permissionRepository.delete("Role", "Manager");
  }
  
  @Test
  public void findAll() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    HashMap<String, Right> rightMap = new HashMap<String, Right>();
    rightMap.put(right.getidName(), right);
    
    Role role = new Role("Manager", rightMap);
    permissionRepository.save("ROLE", role);
    
    List<Role> returnedRole = roleService.findAll();
    
    assertThat("Should have 2 entries", returnedRole.size(), equalTo(2));
    permissionRepository.delete("Role", "Manager");
  }
  
  @Test
  public void addRight() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    HashMap<String, Right> rightMap = new HashMap<String, Right>();
    rightMap.put(right.getidName(), right);
    
    Role role = new Role("Manager", rightMap);
    permissionRepository.save("ROLE", role);
    
    Right rightToAdd = new Right("Billing_Delete", "BILLING.DELETE");
    roleService.addRight(role.getidName(), rightToAdd);
    
    Role returnedRole = permissionRepository.findByIdName("ROLE", role.getidName());
    
    assertThat("Should have 2 entries", returnedRole.getRights().size(), equalTo(2));
    Iterator<Right> roleIterator = returnedRole.getRights().values().iterator();
    assertThat("Should have BILLING.UPDATE", roleIterator.next().getPermission(), equalTo("BILLING.UPDATE"));
    assertThat("Should have BILLING.DELETE", roleIterator.next().getPermission(), equalTo("BILLING.DELETE"));
    permissionRepository.delete("Role", "Manager");
  }
  
  @Test
  public void removeRight() throws Exception {
    Right right = new Right("Billing_Update", "BILLING.UPDATE");
    Right right2 = new Right("Billing_Delete", "BILLING.DELETE");
    HashMap<String, Right> rightMap = new HashMap<String, Right>();
    rightMap.put(right.getidName(), right);
    rightMap.put(right2.getidName(), right2);
    
    Role role = new Role("Manager", rightMap);
    permissionRepository.save("ROLE", role);
    
    roleService.removeRight(role.getidName(), "Billing_Update");
    
    Role returnedRole = permissionRepository.findByIdName("ROLE", role.getidName());
    
    assertThat("Should have 1 entries", returnedRole.getRights().size(), equalTo(1));
    Iterator<Right> roleIterator = returnedRole.getRights().values().iterator(); 
    assertThat("Should have BILLING.DELETE", roleIterator.next().getPermission(), equalTo("BILLING.DELETE"));
    permissionRepository.delete("Role", "Manager");
  }

}
