package com.olmgroup.msp.permissionsCache;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.olmgroup.msp.permissionsCache.model.PermissionType;
import com.olmgroup.msp.permissionsCache.model.Role;

@Configuration
public class RightsCacheApplication {
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration("localhost", 6379);
		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(redisStandaloneConfiguration);
		return jedisConnectionFactory;
	}
	
	@Bean
	RedisTemplate<String, Role> redisTemplate() {
		RedisTemplate<String, Role> redisTemplate = new RedisTemplate<String, Role>();
		
		redisTemplate.setConnectionFactory(jedisConnectionFactory());
		return redisTemplate;
	}
}
