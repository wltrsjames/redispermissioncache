package com.olmgroup.msp.permissionsCache.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.olmgroup.msp.permissionsCache.model.PermissionType;
import com.olmgroup.msp.permissionsCache.model.Role;

@Repository
public class PermissionRepositoryImpl implements PermissionRepository {

	@Autowired
	RedisTemplate<String, Role> redisTemplate;

	public void save(String dataype, Role role) {
		HashOperations<String, String, Role> hashOperation = redisTemplate.opsForHash();
		hashOperation.put(dataype, role.getidName(), role);
	}

	public <T> List<T> findAll(String dataype) {
		HashOperations<String, String, T> hashOperation = redisTemplate.opsForHash();
		return new ArrayList<T>(hashOperation.entries(dataype).values());
	}

	public <T> T findByIdName(String dataype, String idName) {
		HashOperations<String, String, T> hashOperation = redisTemplate.opsForHash();
		return hashOperation.get(dataype, idName);
	}

	public void update(String dataype, Role role) {
		HashOperations<String, String, PermissionType> hashOperation = redisTemplate.opsForHash();
		hashOperation.put(dataype, role.getidName(), role);
		
	}

	public void delete(String dataype, String idName) {
		HashOperations<String, String, PermissionType> hashOperation = redisTemplate.opsForHash();
		hashOperation.delete(dataype, idName);
		
	}
}
