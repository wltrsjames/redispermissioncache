package com.olmgroup.msp.permissionsCache.services;

import java.util.List;
import java.util.Map;

import com.olmgroup.msp.permissionsCache.model.Right;
import com.olmgroup.msp.permissionsCache.model.Role;

public interface RoleService {
	<T extends Role> T getRoleByIdName(String id);
	<T extends Role> List<T> findAll();
	void addRight(String roleIdName, Right right);
	void removeRight(String roleIdName, String right);
	void save(Role role) throws Exception;
	void update(Role role) throws Exception;
	void delete(String id);
}
