package com.olmgroup.msp.permissionsCache.services;

import java.util.List;
import java.util.Map;

import com.olmgroup.msp.permissionsCache.model.PermissionType;
import com.olmgroup.msp.permissionsCache.model.Role;


public interface PermissionRepository {
	void save(String dataType, Role role);
	<T> List<T> findAll(String dataype);
	<T> T findByIdName(String dataType, String idName);
	void update(String dataType, Role role);
	void delete(String dataType, String idName);
}
