package com.olmgroup.msp.permissionsCache.services;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olmgroup.msp.permissionsCache.model.Right;
import com.olmgroup.msp.permissionsCache.model.Role;

@Service
public class RoleServiceImpl implements RoleService {
  public static String ROLE = "ROLE";
  
	@Autowired
	PermissionRepository permissionRepository;

	 public <T extends Role> T getRoleByIdName(String idName) {
	    T role = permissionRepository.findByIdName(ROLE, idName);
	    return role;
	 }
	
	public void save(Role role) throws Exception {
		permissionRepository.save(ROLE, role);
	}
	
	public void addRight(String roleIdName, Right right) {
	  Role role = permissionRepository.findByIdName(ROLE, roleIdName);
	  HashMap<String, Right> existingRights = role.getRights();
	  existingRights.put(right.getidName(), right);
	  	  
	  role.setRights(existingRights);
	  permissionRepository.update(ROLE, role);
	}
	
	public void removeRight(String roleIdName, String rightToRemove) {
	   Role role = permissionRepository.findByIdName(ROLE, roleIdName);
	   HashMap<String, Right> existingRights = role.getRights();
	   existingRights.remove(rightToRemove);
	   
	    role.setRights(existingRights);
	    permissionRepository.update(ROLE, role);
	}
	
	public void update(Role role) throws Exception {
		permissionRepository.update(ROLE, role);
		
	}

	public void delete(String id) {
		permissionRepository.delete(ROLE, id);
		
	}
	
	public <T extends Role> List<T> findAll() {
		List<T> roles = permissionRepository.findAll(ROLE);
		return roles;
	}
}
