package com.olmgroup.msp.permissionsCache.model;

public class Right extends PermissionType {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String permission;

	public Right(String idName, String permission) {
	  super(idName);
		this.permission = permission;
	}

  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }
}