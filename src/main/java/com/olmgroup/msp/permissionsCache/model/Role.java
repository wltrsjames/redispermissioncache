package com.olmgroup.msp.permissionsCache.model;

import java.util.HashMap;
import java.util.Set;

public class Role extends PermissionType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String, Right> rights;
	
	public Role(String idName, HashMap<String, Right> rights) {
		super(idName);
		this.rights = rights;
	}
		
	public HashMap<String, Right> getRights() {
		return rights;
	}
		
	public void setRights(HashMap<String, Right> rights) {
		this.rights = rights;
	}
}
