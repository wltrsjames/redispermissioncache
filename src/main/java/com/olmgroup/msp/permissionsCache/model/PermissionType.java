package com.olmgroup.msp.permissionsCache.model;

import java.io.Serializable;

public abstract class PermissionType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idName;
	
	public PermissionType(String idname) {
	  this.idName = idname;
	}
	
	public String getidName() {
		return idName;
	}
	
	public void setIdName(String idName) {
		this.idName = idName;
	}
}
