# permissions-cache

### This is the Repo for the Redis Permissions cache plugin.

###This maven project can be imported into a microservice like:

``` 
<dependency>
	<groupId>com.olmgroup.msp</groupId>
	<artifactId>permissions-cache</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

### Usage:
```
@Autowired
RoleService roleService;

//save
Right right = new Right("Billing_Add", "BILLING.ADD");
HashMap<String, Right> rightmap = new HashMap<String, Right>();
rightmap.put(right.getidName(), right);
    
Role role = new Role("Admin",rightmap);
roleService.save(role);

//find
Role role = roleService.getRoleByIdName("Admin");

//update
Right rightUpdate = new Right("Billing_Add", "BILLING.ADD"); 
HashMap<String, Right> rightUpdateMap = new HashMap<String, Right>();
rightUpdateMap.put(right.getidName(), rightUpdate);
    
Role roleUpdate = new Role("Manager", rightUpdateMap);
roleService.update(roleUpdate);

//delete
roleService.delete("Manager");

//findAll
List<Role> returnedRole = roleService.findAll();

//addRight
Right rightToAdd = new Right("Billing_Delete", "BILLING.DELETE");
roleService.addRight(role.getidName(), rightToAdd);

//removeRight
roleService.removeRight(role.getidName(), "Billing_Update");
```	